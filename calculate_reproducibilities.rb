#!/bin/env ruby

require 'rubygems'
require 'trollop'
require 'pp'
require File.join(File.dirname(__FILE__), "reproducibility_calculator")

opts = Trollop::options do
  version "calculate_reproducibilities 0.1.0"
  opt :pair_ids, 'Pair identifiers (e.g. "1 2")', :default => "1 2"
  opt :id_type, 'Identifier type: prefix or suffix', :default => "suffix"
  opt :file, "File to process", :type => :string, :required => true
end

pair_names = [opts[:pair_ids].split]
id_type = opts[:id_type].to_sym

c = ReproducibilityCalculator.new(:pair_identifiers => pair_names, 
		:identifier_type => id_type, :filename => opts[:file])

c.calculate_reproducibilities
