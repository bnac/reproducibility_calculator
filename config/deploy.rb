#http://ctrlshiftesc.wordpress.com/2008/01/02/solution-to-capistrano-issue/
default_run_options[:pty] = true
 
SVN_LOCATION = `svn info`.grep(/URL/).first.split.last
 
set :vr, "vr1"
set :application, "reproducibility_calculator"
set :repository, "git@github.com:WeirdAlchemy/reproducibility_calculator.git"
set :scm, "git"
ssh_options[:forward_agent] = true

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/shared/software/#{vr}/bnac/#{application}"

  
role :app, "localhost"
 
namespace :deploy do
  # Overwritten to provide flexibility for people who aren't using Rails.
  task :setup, :except => { :no_release => true } do
    dirs = [deploy_to, releases_path, shared_path]
    dirs += %w(system).map { |d| File.join(shared_path, d) }
    run "umask 02 && mkdir -p #{dirs.join(' ')}" 
  end
 
  # Also overwritten to remove Rails-specific code.
  task :finalize_update, :except => { :no_release => true } do
    run "chmod -R g+w #{release_path}" if fetch(:group_writable, true)
  end
 
 
# Each of the following tasks are Rails specific. They're removed.
  task :migrate do
  end
  task :migrations do
  end
  task :cold do
  end
  task :start do
  end
  task :stop do
  end
  task :restart do
  end
end

