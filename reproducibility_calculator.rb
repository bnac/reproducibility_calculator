# This project provides a ReproducibilityCalculator utility class to calculate
# pair-wise reproducibilities from an SPSS file. 
#
# It is not a stand-alone program, but is rather intended to be called from
# a short ruby script that sets up the appropriate input information. In the 
# future it may include an input parser and be made stand-alone, but the current
# need for this is minimal.
#
# pair_names = [
#	[".1", ".2"],
#]
#
#c = ReproducibilityCalculator.new(:pair_identifiers => pair_names, 
#		:identifier_type => :suffix, :filename => 'srnew.sav')
#
#c.calculate_reproducibilities
#
# See the ReproducibilityCalculator class itself for further information.
#
# Note that in order to use this utility you will need to have R and the 
# rsruby gem installed. On Ubuntu, you'll need to do the following:
# * sudo aptitude install r-base r-base-dev
# * sudo gem install rsruby -- --with-R-dir=/usr/lib/R --with-R-include=/usr/share/R/include
# On other systems you may need to modify the package commands and specified 
# directories.

require 'rubygems'
require 'rsruby'

ENV["R_HOME"] = "/usr/lib/R"

# ReproducibilityCalculator takes an SPSS or Excel file and calculates both categorical
# and numeric reproducibilities. Categorical reproducibilities are calculated
# using Cohen's Kappa and straight agreement (%). Numeric reproducibilities are
# calculated using both ICC and Kendall's W. The R irr package is used to 
# perform the actual calculations via RSRuby (R embedded in Ruby).
#
# Pair identifiers (either prefixes or suffixes) are passed in, and the 
# calculator will then look for all variables pairs matching the given patterns.
# For example, given the identifier set [[".1", ".2"]] it will find "test_a.1" 
# and "test_a.2" as a pair and calculate the reproducibility between them.
#
# The calculator will also attempt to determine whether the variables pairs are 
# categorical or numeric based on the number of uniqe entries (1-2 for 
# categorical, >2 for numeric). This behavior can be overridden by specifying
# explicit patterns for each type.
class ReproducibilityCalculator

	attr_reader :r
	attr_reader :filename
	attr_reader :pair_identifiers
	attr_reader :identifier_type
	attr_reader :categorical_regexp_override
	attr_reader :numeric_regexp_override
	attr_reader :use_kendall

	attr_accessor :categoricals
	attr_accessor :numerics


	# Create a new ReproducibilityCalculator. A parameters hash is required, which
	# must at least specify:
	# [:filename] The path to the SPSS file to use (should have .sav extension)
	#
	# In addition, the following optional parameters can be provided:
	# [:pair_identifiers] An array of 2-element arrays, each specifying a pair 
	#                     of identifier patterns (default [[".1", ".2"]])
	# [:identifier_type] Either :prefix or :suffix (default :suffix)
	# [:categorical_regexp_override]
	#   A regexp which when matched to a variable name will force it to be
	#   interpreted as categorical.
	# [:numeric_regexp_override]
	#   A regexp which when matched to a variable name will force it to be
	#   interpreted as numeric.
	def initialize(params = {})
		defaults = {
			:pair_identifiers => [[".1",".2"]],
			:identifier_type => :suffix,
			:use_kendall => true
		}
		params = defaults.merge(params)
		@pair_identifiers = params[:pair_identifiers]
		@identifier_type = params[:identifier_type]
		@filename = params[:filename]
		@categorical_regexp_override = params[:categorical_regexp_override]
		@numeric_regexp_override = params[:numeric_regexp_override]
		@use_kendall = params[:use_kendall]
		@r = r = RSRuby.instance
		self.categoricals = {}
		self.numerics = {}
		pair_identifiers.each do |pair|
			categoricals[pair] = []
			numerics[pair] = []
		end
	end


	# Calculates the reproducibilities from the provided file given the options
	# specified at creation. Outputs everything to the screen in tabular format
	# separated by tabs. Redirect to a CSV and open in Excel/OOCalc to create
	# an actual spreadsheet.
	def calculate_reproducibilities
		read_file(filename)
		find_target_pairs
		r.require('irr')
		calculate_kappas
		calculate_agreements
		calculate_iccs
		calculate_kendall_ws if use_kendall
		correct_comparisons
		print_results
	end



	private


	def read_file(filename)
          if filename =~ /\.sav$/
            read_spss(filename)
          elsif filename =~ /\.csv$/
            read_csv(filename)
          else
            raise RuntimeError.new("Unknown filetype for #{filename}")
          end
        end

        def read_spss(filename)
		puts "Reading in SPSS data file #{filename}"
		r.require('foreign')
		r.eval_R("data = read.spss('#{filename}',to.data.frame=TRUE," +
				"use.value.labels=FALSE,use.missings=TRUE)")
	end

        def read_csv(filename)
		puts "Reading in CSV data file #{filename}"
		r.eval_R("data = read.csv('#{filename}')")
        end
        

	# Find the variable pairs that should be tested for agreement. We do this
	# based on the provided prefixes/suffixes.
	def find_target_pairs

		puts "Processing variable names."

		# First get the eligible base names
		r.eval_R("vars = names(data)")
		vars = r.vars
		basenames = vars.collect{ |v|
			final_name = nil
			pair_identifiers.each do |p|
				p.each do |n|
					if identifier_type == :prefix and v =~ /^#{n}.*/
						final_name = v.gsub(/^#{n}/,"")
					elsif identifier_type == :suffix and v =~ /.*#{n}$/
						final_name = v.gsub(/#{n}$/,"")
					end
				end
			end
			final_name
		}.compact.uniq



		# Then check whether each specified pair exists with each given basename,
		# and if so put it in the list for calculation.
		basenames.each do |basename|
			pair_identifiers.each do |pair_identifier|
				v1 = fullname(basename, pair_identifier[0])
				v2 = fullname(basename, pair_identifier[1])
				if vars.include?(v1) and vars.include?(v2)
						pt = pairtype(basename,v1,v2)
						if pt == :categorical
							categoricals[pair_identifier] << [basename,v1,v2,{}]
						elsif pt == :numeric
							numerics[pair_identifier] << [basename,v1,v2,{}]
						else
							puts "No data in #{basename} for pair [#{pair_identifier.join(',')}]"
						end
				end
			end
		end

	end


	def calculate_kappas
		puts "Calculating kappas."
		pair_identifiers.each do |pair_identifier|
			categoricals[pair_identifier].each do |varpair|
				varpair[3]["kappa"] = kappa(varpair[1], varpair[2])
			end
		end
	end


	def kappa(varname1, varname2)
		r.delete_from_cache("k")
		r.eval_R("k <- kappa2(data[c('#{varname1}','#{varname2}')])")
		return r.k
	end



	def calculate_agreements
		puts "Calculating agreements."
		pair_identifiers.each do |pair_identifier|
			categoricals[pair_identifier].each do |varpair|
				varpair[3]["agreement"] = agreement(varpair[1], varpair[2])
			end
		end
	end


	def agreement(varname1, varname2)
		r.delete_from_cache("a")
		r.eval_R("a <- agree(data[c('#{varname1}','#{varname2}')])")
		return r.a
	end

		
	def calculate_iccs
		puts "Calculating ICCs."
		pair_identifiers.each do |pair_identifier|
			numerics[pair_identifier].each do |varpair|
				varpair[3]["icc"] = icc(varpair[1], varpair[2])
			end
		end
	end


	def icc(varname1, varname2)
		r.delete_from_cache("i")
		r.eval_R("i <- icc(data[c('#{varname1}','#{varname2}')],"+
				"model='twoway',type='agreement')")
		return r.i
	end


	def calculate_kendall_ws
		puts "Calculating Kendall W's."
		pair_identifiers.each do |pair_identifier|
			numerics[pair_identifier].each do |varpair|
				varpair[3]["kendall"] = kendall(varpair[1], varpair[2])
			end
		end
	end


	def kendall(varname1, varname2)
		r.delete_from_cache("k")
		r.eval_R("k <- kendall(data[c('#{varname1}','#{varname2}')],"+
				"correct=TRUE)")
		return r.k
	end

	
	def correct_comparisons
		p_values = []
		categorical_basenames = categoricals.first.last.collect{|l| l[0]}
		categorical_basenames.each_with_index do |basename, i|
			pair_identifiers.each do |pair_identifier|
				entry = categoricals[pair_identifier].detect{|e| e[0] == basename}
				p_value = entry[3]["kappa"]["p.value"].to_f
				unless p_value.nan? or p_value.nil?
					p_values << p_value
				end
			end
		end
		numeric_basenames = numerics.first.last.collect{|l| l[0]}
		numeric_basenames.each_with_index do |basename, i|
			pair_identifiers.each do |pair_identifier|
				entry = numerics[pair_identifier].detect{|e| e[0] == basename}
				p_value = entry[3]["icc"]["p.value"].to_f
				unless p_value.nan? or p_value.nil?
					p_values << p_value
				end
				if use_kendall
					p_value = entry[3]["kendall"]["p.value"].to_f
					unless p_value.nan? or p_value.nil?
						p_values << p_value
					end
				end
			end
		end

		r.assign('pvec', p_values)
		r.eval_R("qvec <- p.adjust(pvec, method='fdr')")
		q_values = r.qvec
		
		q_lookup = {}
		p_values.each_with_index do |p,i| 
			q_lookup[p] = q_values[i]
		end

		categorical_basenames = categoricals.first.last.collect{|l| l[0]}
		categorical_basenames.each_with_index do |basename, i|
			pair_identifiers.each do |pair_identifier|
				entry = categoricals[pair_identifier].detect{|e| e[0] == basename}
				p_value = entry[3]["kappa"]["p.value"].to_f
				entry[3]["kappa"]["q.value"] = q_lookup[p_value]
			end
		end
		numeric_basenames = numerics.first.last.collect{|l| l[0]}
		numeric_basenames.each_with_index do |basename, i|
			pair_identifiers.each do |pair_identifier|
				entry = numerics[pair_identifier].detect{|e| e[0] == basename}
				p_value = entry[3]["icc"]["p.value"].to_f
				entry[3]["icc"]["q.value"] = q_lookup[p_value]
				if use_kendall
					p_value = entry[3]["kendall"]["p.value"].to_f
					entry[3]["kendall"]["q.value"] = q_lookup[p_value]
				end
			end
		end

	end


	def print_results
		puts "Categorical variables"
		categorical_basenames = categoricals.first.last.collect{|l| l[0]}
		puts ["Measure", pair_identifiers.collect{|p| [p.join(" "), "", "", "",]}].
				flatten.join("\t")
		puts ["", ["Kappa", "p", "q", "Agreement %"] * pair_identifiers.size].
				flatten.join("\t")
		categorical_basenames.each_with_index do |basename, i|
			line = [basename]
			pair_identifiers.each do |pair_identifier|
				entry = categoricals[pair_identifier].detect{|e| e[0] == basename}
				line << format_regular(entry[3]["kappa"]["value"])
				line << format_p(entry[3]["kappa"]["p.value"])
				line << format_p(entry[3]["kappa"]["q.value"])
				line << format_regular(entry[3]["agreement"]["value"])
			end
			puts line.join("\t")
		end

		puts

		puts "Numeric variables"
		numeric_basenames = numerics.first.last.collect{|l| l[0]}
		spacer = use_kendall ? [""] * 5 : [""] * 2
		puts ["Measure", pair_identifiers.collect{|p| [p.join(" ")] + spacer}].
				flatten.join("\t")
		if use_kendall
			puts ["", ["ICC", "p", "q", "Kendall W","p", "q"] * pair_identifiers.size].
					flatten.join("\t")
		else
			puts ["", ["ICC", "p", "q"] * pair_identifiers.size].
					flatten.join("\t")
		end
		numeric_basenames.each_with_index do |basename, i|
			line = [basename]
			pair_identifiers.each do |pair_identifier|
				entry = numerics[pair_identifier].detect{|e| e[0] == basename}
				line << format_regular(entry[3]["icc"]["value"])
				line << format_p(entry[3]["icc"]["p.value"])
				line << format_p(entry[3]["icc"]["q.value"])
				if use_kendall
					line << format_regular(entry[3]["kendall"]["value"])
					line << format_p(entry[3]["kendall"]["p.value"])
					line << format_p(entry[3]["kendall"]["q.value"])
				end
			end
			puts line.join("\t")
		end
	end


	# Some basic utility functions. 

	# Unfortunately, importing an SPSS file with labels->categorical on (making R 
  # factors) causes _any_ variables with value labels to be turned into factors. 
	# This is wrong in most cases based on the way BNAC uses SPSS (with missing 
	# values as value labels). So, we have to leave everything numeric on the way 
	# in. However, we need some way to tell if variables are really categorical in 
	# order to decide the type of statistical test to use. So, we do it 
	# heuristically based on the number of unique values.
	def vartype(var)
		var.uniq.size > 2 ? :numeric : :categorical
	end


	def pairtype(basename, varname1, varname2)

		if categorical_regexp_override and basename =~ categorical_regexp_override
			return :categorical
		end

		if numeric_regexp_override and basename =~ numeric_regexp_override
			return :numeric
		end

		r.delete_from_cache("v1")
		r.delete_from_cache("v2")
		r.eval_R("v1 <- data[c('#{varname1}')]")		
		r.eval_R("v2 <- data[c('#{varname2}')]")		
		var1 = r.v1.values[0].select{|v| not v.nan?}
		var2 = r.v2.values[0].select{|v| not v.nan?}
		if var1.size < 2 or var2.size < 2
			return :empty
		end
		if vartype(var1) == :categorical and vartype(var2) == :categorical
			return :categorical
		else
			return :numeric
		end
	end


	# Convenience method since we don't know a-priori whether we'll be dealing
	# with prefixes or suffixes. Just prevents a lot of repetition of the same
	# code.
	def fullname(basename, identifier)
		if identifier_type == :prefix
			"#{identifier}#{basename}"
		else
			"#{basename}#{identifier}"
		end
	end


	def format_p(p)
		p_f = p.to_f
		return "N/A" if p_f.nan? or p.nil?
		return "<0.001" if p_f < 0.001 
		return '%.3f' % p_f
	end

	
	def format_regular(n)
		n_f = n.to_f
		return "N/A" if n_f.nan?
		return '%.3f' % n_f
	end

end








